execute as @e[type=item,nbt={Item:{id:"minecraft:paper"}}] at @s if block ~ ~ ~ minecraft:player_head{PublicBukkitValues:{"slimefun:slimefun_block":"VANILLA_AUTO_CRAFTER"}} if entity @e[type=item,nbt={Item:{id:"minecraft:gunpowder"}},nbt=!{Item:{tag:{UsedForFirework:1}}},distance=..2] run summon minecraft:item ~ ~ ~ {Item:{id:"minecraft:firework_rocket",tag:{Fireworks:{Flight:1b}},Count:1b}}

execute as @e[type=item,nbt={Item:{id:"minecraft:firework_rocket"},Age:0s}] at @s if block ~ ~ ~ minecraft:player_head{PublicBukkitValues:{"slimefun:slimefun_block":"VANILLA_AUTO_CRAFTER"}} as @e[type=item,limit=1,sort=nearest,distance=..2,nbt={Item:{id:"minecraft:gunpowder"}},nbt=!{Item:{tag:{UsedForFirework:1}}}] store result score @s firework_gunpowder_stack run data get entity @s Item.Count

execute as @e[type=item,nbt={Item:{id:"minecraft:firework_rocket"},Age:0s}] at @s if block ~ ~ ~ minecraft:player_head{PublicBukkitValues:{"slimefun:slimefun_block":"VANILLA_AUTO_CRAFTER"}} as @e[type=item,limit=1,sort=nearest,distance=..2,nbt={Item:{id:"minecraft:gunpowder"}},nbt=!{Item:{tag:{UsedForFirework:1}}}] store result entity @s Item.Count byte 1 run scoreboard players remove @s firework_gunpowder_stack 1

execute as @e[type=item,nbt={Item:{id:"minecraft:firework_rocket"},Age:0s}] at @s if block ~ ~ ~ minecraft:player_head{PublicBukkitValues:{"slimefun:slimefun_block":"VANILLA_AUTO_CRAFTER"}} as @e[type=item,limit=1,sort=nearest,distance=..2,nbt={Item:{id:"minecraft:paper"}}] store result score @s firework_paper_stack run data get entity @s Item.Count

execute as @e[type=item,nbt={Item:{id:"minecraft:firework_rocket"},Age:0s}] at @s if block ~ ~ ~ minecraft:player_head{PublicBukkitValues:{"slimefun:slimefun_block":"VANILLA_AUTO_CRAFTER"}} as @e[type=item,limit=1,sort=nearest,distance=..2,nbt={Item:{id:"minecraft:paper"}}] store result entity @s Item.Count byte 1 run scoreboard players remove @s firework_paper_stack 1



execute as @e[type=item,nbt={Item:{id:"minecraft:firework_rocket",tag:{Fireworks:{Flight:1b}}}}] unless entity @s[nbt={Age:0s}] at @s if block ~ ~ ~ minecraft:player_head{PublicBukkitValues:{"slimefun:slimefun_block":"VANILLA_AUTO_CRAFTER"}} if entity @e[type=item,limit=1,sort=nearest,distance=..2,nbt={Item:{id:"minecraft:gunpowder"}},nbt=!{Item:{tag:{UsedForFirework:1}}}] run summon minecraft:item ~ ~ ~ {Item:{id:"minecraft:firework_rocket",tag:{Fireworks:{Flight:2b}},Count:1b}}

execute as @e[type=item,nbt={Item:{id:"minecraft:firework_rocket",tag:{Fireworks:{Flight:1b}}}}] unless entity @s[nbt={Age:0s}] at @s if block ~ ~ ~ minecraft:player_head{PublicBukkitValues:{"slimefun:slimefun_block":"VANILLA_AUTO_CRAFTER"}} if entity @e[type=item,limit=1,sort=nearest,distance=..2,nbt={Item:{id:"minecraft:gunpowder"}},nbt=!{Item:{tag:{UsedForFirework:1}}}] run data modify entity @e[type=item,limit=1,sort=nearest,distance=..2,nbt={Item:{id:"minecraft:firework_rocket",tag:{Fireworks:{Flight:2b}},Count:1b}}] Item.tag.Fireworks.Explosions set from entity @s Item.tag.Fireworks.Explosions

execute as @e[type=item,nbt={Item:{id:"minecraft:firework_rocket",tag:{Fireworks:{Flight:1b}}}}] unless entity @s[nbt={Age:0s}] at @s if block ~ ~ ~ minecraft:player_head{PublicBukkitValues:{"slimefun:slimefun_block":"VANILLA_AUTO_CRAFTER"}} if entity @e[type=item,limit=1,sort=nearest,distance=..2,nbt={Item:{id:"minecraft:gunpowder"}},nbt=!{Item:{tag:{UsedForFirework:1}}}] store result score @s firework_firework_stack run data get entity @s Item.Count

execute as @e[type=item,nbt={Item:{id:"minecraft:firework_rocket",tag:{Fireworks:{Flight:1b}}}}] unless entity @s[nbt={Age:0s}] at @s if block ~ ~ ~ minecraft:player_head{PublicBukkitValues:{"slimefun:slimefun_block":"VANILLA_AUTO_CRAFTER"}} as @e[type=item,limit=1,sort=nearest,distance=..2,nbt={Item:{id:"minecraft:gunpowder"}},nbt=!{Item:{tag:{UsedForFirework:1}}}] store result score @s firework_gunpowder_stack run data get entity @s Item.Count

execute as @e[type=item,nbt={Item:{id:"minecraft:firework_rocket",tag:{Fireworks:{Flight:1b}}}}] unless entity @s[nbt={Age:0s}] at @s if block ~ ~ ~ minecraft:player_head{PublicBukkitValues:{"slimefun:slimefun_block":"VANILLA_AUTO_CRAFTER"}} as @e[type=item,limit=1,sort=nearest,distance=..2,nbt={Item:{id:"minecraft:gunpowder"}},nbt=!{Item:{tag:{UsedForFirework:1}}}] run data modify entity @s Item.tag.UsedForFirework set value 1

execute as @e[type=item,nbt={Item:{id:"minecraft:firework_rocket",tag:{Fireworks:{Flight:1b}}}}] unless entity @s[nbt={Age:0s}] at @s if block ~ ~ ~ minecraft:player_head{PublicBukkitValues:{"slimefun:slimefun_block":"VANILLA_AUTO_CRAFTER"}} if entity @e[type=item,limit=1,sort=nearest,distance=..2,nbt={Item:{id:"minecraft:gunpowder"}}] store result entity @s Item.Count byte 1 run scoreboard players remove @s firework_firework_stack 1

execute as @e[type=item,nbt={Item:{id:"minecraft:gunpowder",tag:{UsedForFirework:1}}}] store result entity @s Item.Count byte 1 run scoreboard players remove @s firework_gunpowder_stack 1

execute as @e[type=item,nbt={Item:{id:"minecraft:gunpowder",tag:{UsedForFirework:1}}}] run data modify entity @s Item.tag.UsedForFirework set value 0



execute as @e[type=item,nbt={Item:{id:"minecraft:firework_rocket",tag:{Fireworks:{Flight:2b}}}}] unless entity @s[nbt={Age:0s}] at @s if block ~ ~ ~ minecraft:player_head{PublicBukkitValues:{"slimefun:slimefun_block":"VANILLA_AUTO_CRAFTER"}} if entity @e[type=item,limit=1,sort=nearest,distance=..2,nbt={Item:{id:"minecraft:gunpowder"}},nbt=!{Item:{tag:{UsedForFirework:1}}}] run summon minecraft:item ~ ~ ~ {Item:{id:"minecraft:firework_rocket",tag:{Fireworks:{Flight:3b}},Count:1b}}

execute as @e[type=item,nbt={Item:{id:"minecraft:firework_rocket",tag:{Fireworks:{Flight:2b}}}}] unless entity @s[nbt={Age:0s}] at @s if block ~ ~ ~ minecraft:player_head{PublicBukkitValues:{"slimefun:slimefun_block":"VANILLA_AUTO_CRAFTER"}} if entity @e[type=item,limit=1,sort=nearest,distance=..2,nbt={Item:{id:"minecraft:gunpowder"}},nbt=!{Item:{tag:{UsedForFirework:1}}}] run data modify entity @e[type=item,limit=1,sort=nearest,distance=..2,nbt={Item:{id:"minecraft:firework_rocket",tag:{Fireworks:{Flight:3b}},Count:1b}}] Item.tag.Fireworks.Explosions set from entity @s Item.tag.Fireworks.Explosions

execute as @e[type=item,nbt={Item:{id:"minecraft:firework_rocket",tag:{Fireworks:{Flight:2b}}}}] unless entity @s[nbt={Age:0s}] at @s if block ~ ~ ~ minecraft:player_head{PublicBukkitValues:{"slimefun:slimefun_block":"VANILLA_AUTO_CRAFTER"}} if entity @e[type=item,limit=1,sort=nearest,distance=..2,nbt={Item:{id:"minecraft:gunpowder"}},nbt=!{Item:{tag:{UsedForFirework:1}}}] store result score @s firework_firework_stack run data get entity @s Item.Count

execute as @e[type=item,nbt={Item:{id:"minecraft:firework_rocket",tag:{Fireworks:{Flight:2b}}}}] unless entity @s[nbt={Age:0s}] at @s if block ~ ~ ~ minecraft:player_head{PublicBukkitValues:{"slimefun:slimefun_block":"VANILLA_AUTO_CRAFTER"}} as @e[type=item,limit=1,sort=nearest,distance=..2,nbt={Item:{id:"minecraft:gunpowder"}},nbt=!{Item:{tag:{UsedForFirework:1}}}] store result score @s firework_gunpowder_stack run data get entity @s Item.Count

execute as @e[type=item,nbt={Item:{id:"minecraft:firework_rocket",tag:{Fireworks:{Flight:2b}}}}] unless entity @s[nbt={Age:0s}] at @s if block ~ ~ ~ minecraft:player_head{PublicBukkitValues:{"slimefun:slimefun_block":"VANILLA_AUTO_CRAFTER"}} as @e[type=item,limit=1,sort=nearest,distance=..2,nbt={Item:{id:"minecraft:gunpowder"}},nbt=!{Item:{tag:{UsedForFirework:1}}}] run data modify entity @s Item.tag.UsedForFirework set value 1

execute as @e[type=item,nbt={Item:{id:"minecraft:firework_rocket",tag:{Fireworks:{Flight:2b}}}}] unless entity @s[nbt={Age:0s}] at @s if block ~ ~ ~ minecraft:player_head{PublicBukkitValues:{"slimefun:slimefun_block":"VANILLA_AUTO_CRAFTER"}} if entity @e[type=item,limit=1,sort=nearest,distance=..2,nbt={Item:{id:"minecraft:gunpowder"}}] store result entity @s Item.Count byte 1 run scoreboard players remove @s firework_firework_stack 1

execute as @e[type=item,nbt={Item:{id:"minecraft:gunpowder",tag:{UsedForFirework:1}}}] store result entity @s Item.Count byte 1 run scoreboard players remove @s firework_gunpowder_stack 1

execute as @e[type=item,nbt={Item:{id:"minecraft:gunpowder",tag:{UsedForFirework:1}}}] run data modify entity @s Item.tag.UsedForFirework set value 0























